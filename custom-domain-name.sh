#! /bin/bash

clear

choice=y;

#Autor: gg / yans

while [ "$choice" = "y" ]; do

echo
echo
echo
echo "   ----------------------------------------------------"
echo "   --------------   custom domain name   --------------"
echo "   ----------------------------------------------------"
echo

echo "    0. Create folder in /var/www/html/"
echo "    1. Edit /etc/hosts"
echo "    2. Create a "*.conf" file in /etc/apache2/sites-available"
echo "    3. Edit new created file"
echo "    4. Enable domain"
echo "    5. Reload apache2"
echo "    6. Add user to www-data:www-data group"
echo "    7. Set 755 permission 755"
echo "    8. List the files in /etc/apache2/sites-available"
echo "    9. Create & set phpinfo file in to your folder"

echo
echo "   To close program press the 'q' key"
echo
echo -n "   Choose number.: "

read a

case "$a" in

"0")
      echo -n $folder "   Create folder : "
      read folder
      sudo mkdir /var/www/html/$folder

      echo "   Folder $folder was created" ;;
"1")
      sudo nano /etc/hosts ;;

"2")
      echo -n $file "   Create file : "
      read file
      sudo touch /etc/apache2/sites-available/$file
      sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/$file
      echo "   File $file was created" ;;

"3")
      sudo nano /etc/apache2/sites-available/$file
      echo "   File $file was modified" ;;
"4")
      sudo a2ensite $file
      echo "   Domain $folder was enabled" ;;
"5")
      sudo systemctl reload apache2
      echo "   Apache2 was reloaded" ;;
"6")
      sudo chown -R www-data:www-data /var/www/html/$folder
      echo "   Your username has been added as a member of www-data group in $folder" ;;
"7")
      sudo chmod 755 -R /var/www/html/$folder
      echo "   The permissions of $folder was changed" ;;
"8")
      echo
      ls -la /etc/apache2/sites-available/
      echo
      $file ;;
"9")
      cd /home/awakening/Dev/html/$folder
      sudo touch index.php
      echo "<?php echo phpinfo(); ?>" > index.php | sudo chmod 755 /home/awakening/Dev/html/$folder/index.php
      echo
      echo "   File index.php was created!!!"
      echo "   Go to your browser" ;;

"q")
      echo
      echo "   You are free as a free software is!"
      echo
      exit 1 ;;
*)
      echo
      echo "   Uuuuuuu! Incorrect option, incorrect thought!"
      echo "      Good luck..."


esac

echo
echo -n "   Do you want to perform any other task? [ y/n ] : "
read choice
clear
done

exit 0
